namespace EFL.Oracle
{
    using System.Data.Entity;
    using System.Data.Common;
    using LogMgr;

    [DbConfigurationType(typeof(EFL.Db.Common.DefaultSqlEFConfiguration))]
    public partial class OracleContext : DbContext
    {
        public OracleContext(DbConnection connection)
            //: base("name=OracleModel")
            : base(connection, true)
        {
            this.Database.Log = s => new Log(ScopeEnum.RawOracle).RawORACLE(s);
        }

        public virtual DbSet<department> departments { get; set; }
        public virtual DbSet<dept_emp> dept_emp { get; set; }
        public virtual DbSet<dept_manager> dept_manager { get; set; }
        public virtual DbSet<employee> employees { get; set; }
        public virtual DbSet<salary> salaries { get; set; }
        public virtual DbSet<title> titles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<department>()
                .Property(e => e.dept_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.dept_name)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .HasMany(e => e.dept_emp)
                .WithRequired(e => e.department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<department>()
                .HasMany(e => e.dept_manager)
                .WithRequired(e => e.department)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<dept_emp>()
                .Property(e => e.dept_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<dept_manager>()
                .Property(e => e.dept_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.gender)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .HasMany(e => e.dept_emp)
                .WithRequired(e => e.employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<employee>()
                .HasMany(e => e.dept_manager)
                .WithRequired(e => e.employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<employee>()
                .HasMany(e => e.salaries)
                .WithRequired(e => e.employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<employee>()
                .HasMany(e => e.titles)
                .WithRequired(e => e.employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<title>()
                .Property(e => e.emp_title)
                .IsUnicode(false);
        }
    }
}
