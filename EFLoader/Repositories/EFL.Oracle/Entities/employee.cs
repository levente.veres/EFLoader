namespace EFL.Oracle
{
    using IEntity;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMPLOYEES.EMPLOYEES")]
    public partial class employee 
        {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public employee()
        {
            dept_emp = new HashSet<dept_emp>();
            dept_manager = new HashSet<dept_manager>();
            salaries = new HashSet<salary>();
            titles = new HashSet<title>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("EMP_NO")]
        public int emp_no { get; set; }

        [Column("BIRTH_DATE")]
        public DateTime birth_date { get; set; }

        [Required]
        [StringLength(14)]
        [Column("FIRST_NAME")]
        public string first_name { get; set; }

        [Required]
        [StringLength(16)]
        [Column("LAST_NAME")]
        public string last_name { get; set; }

        [Required]
        [StringLength(4000)]
        [Column("GENDER")]
        public string gender { get; set; }
        [Column("HIRE_DATE")]
        public DateTime hire_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dept_emp> dept_emp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dept_manager> dept_manager { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<salary> salaries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<title> titles { get; set; }
    }
}
