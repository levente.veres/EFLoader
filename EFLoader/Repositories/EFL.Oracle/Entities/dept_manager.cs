namespace EFL.Oracle
{
    using IEntity;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMPLOYEES.DEPT_MANAGER")]
    public partial class dept_manager 
    {
        [Key]
        [Column("DEPT_NO", Order = 0)]
        [StringLength(4)]
        public string dept_no { get; set; }

        [Key]
        [Column("EMP_NO", Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int emp_no { get; set; }
        [Column("FROM_DATE")]
        public DateTime from_date { get; set; }
        [Column("TO_DATE")]
        public DateTime to_date { get; set; }
        [Column("DEPARTMENT")]
        public virtual department department { get; set; }
        [Column("EMPLOYEE")]
        public virtual employee employee { get; set; }
    }
}
