namespace EFL.Oracle
{
    using IEntity;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMPLOYEES.SALARIES")]
    public partial class salary 
    {
        [Key]
        [Column("EMP_NO", Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int emp_no { get; set; }

        [Column("SALARY")]
        public int emp_salary { get; set; }

        [Key]
        [Column("FROM_DATE", Order = 1)]
        public DateTime from_date { get; set; }
        [Column("TO_DATE")]
        public DateTime to_date { get; set; }

        public virtual employee employee { get; set; }
    }
}
