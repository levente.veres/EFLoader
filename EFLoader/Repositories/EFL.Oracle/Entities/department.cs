namespace EFL.Oracle
{
    using IEntity;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EMPLOYEES.DEPARTMENTS")]
    public partial class department
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public department()
        {
            dept_emp = new HashSet<dept_emp>();
            dept_manager = new HashSet<dept_manager>();
        }

        [Key]
        [StringLength(4)]
        [Column("DEPT_NO")]
        public string dept_no { get; set; }

        [Required]
        [StringLength(40)]
        [Column("DEPT_NAME")]
        public string dept_name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dept_emp> dept_emp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dept_manager> dept_manager { get; set; }
    }
}
