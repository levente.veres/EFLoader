namespace EFL.MSSQL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Common;
    using Db.Common;
    using IEntity;
    using IEntity.Entities;
    using IEntity.Entities.Base;
    using abstr = EFL.IEntity.Entities.Base;
    using MSSQL;
    using System.Diagnostics;
    using EFL.LogMgr;

    [DbConfigurationType(typeof(DefaultSqlEFConfiguration))]
    public partial class MsSqlContext : DbContext
    {
        public MsSqlContext(DbConnection connection) : base(connection, true)
        //: base("name=MsSqlModel") 
        {
            this.Database.Log = s => new Log(ScopeEnum.RawMsSQL).RawMSSQL(s);      
        }


        public virtual DbSet<department> departments { get; set; }
        public virtual DbSet<dept_emp> dept_emp { get; set; }
        public virtual DbSet<dept_manager> dept_manager { get; set; }
        public virtual DbSet<employee> employees { get; set; }
        public virtual DbSet<salary> salaries { get; set; }
        public virtual DbSet<title> titles { get; set; }


        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        Console.WriteLine(message);
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<department>()
                .Property(e => e.dept_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.dept_name)
                .IsUnicode(false);


            modelBuilder.Entity<dept_emp>()
                .Property(e => e.dept_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<dept_manager>()
                .Property(e => e.dept_no)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.gender)
                .IsUnicode(false);

            modelBuilder.Entity<title>()
                .Property(e => e.emp_title)
                .IsUnicode(false);
        }

    }
}
