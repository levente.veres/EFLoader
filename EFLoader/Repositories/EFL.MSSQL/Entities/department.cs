using EFL.IEntity;
using EFL.IEntity.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using abstr = EFL.IEntity.Entities.Base;

namespace EFL.MSSQL
{

    public partial class department
        //: abstr.department
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public department()
        {
            dept_emp = new HashSet<dept_emp>();
            dept_manager = new HashSet<dept_manager>();
        }

        [Key]
        [StringLength(4)]
        public string dept_no { get; set; }

        [Required]
        [StringLength(40)]
        public string dept_name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dept_emp> dept_emp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dept_manager> dept_manager { get; set; }
    }
}
