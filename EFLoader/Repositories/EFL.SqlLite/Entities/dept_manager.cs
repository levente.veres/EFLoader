namespace EFL.SqlLite
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class dept_manager
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(4)]
        public string dept_no { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long emp_no { get; set; }

        public DateTime from_date { get; set; }

        public DateTime to_date { get; set; }

        public virtual department department { get; set; }

        public virtual employee employee { get; set; }
    }
}
