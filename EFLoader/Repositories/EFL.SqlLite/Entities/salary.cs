namespace EFL.SqlLite
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class salary
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long emp_no { get; set; }

        [Column("salary")]
        public long emp_salary { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime from_date { get; set; }

        public DateTime to_date { get; set; }

        public virtual employee employee { get; set; }
    }
}
