namespace EFL.SqlLite
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class title
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long emp_no { get; set; }

        [Key]
        [Column("title", Order = 1)]
        [StringLength(50)]
        public string emp_title { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime from_date { get; set; }

        public DateTime? to_date { get; set; }

        public virtual employee employee { get; set; }
    }
}
