﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.IEntity.Entities.Base
{
    public class department 
    {
        public department()
        {
            dept_emp = new HashSet<dept_emp>();
            dept_manager = new HashSet<dept_manager>();
        }
        
        public string dept_no { get; set; }

        
        public string dept_name { get; set; }

        
        public virtual ICollection<dept_emp> dept_emp { get; set; }

        
        public virtual ICollection<dept_manager> dept_manager { get; set; }

    }
}
