﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.IEntity.Entities.Base
{
    public class title 
    {
        public int emp_no { get; set; }

        public string emp_title { get; set; }

        public DateTime from_date { get; set; }

        public DateTime? to_date { get; set; }

        public virtual employee employee { get; set; }
    }
}
