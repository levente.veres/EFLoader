﻿using EFL.IEntity.Entities;
using EFL.IEntity.Entities.Base;
using System.Data.Entity;

namespace EFL.IEntity
{
    public interface IDataContext
    {
        DbSet<Idepartment> departments { get;} 
        DbSet<Idept_emp> dept_emp { get; }
        DbSet<Idept_manager> dept_manager { get; }
        DbSet<Iemployee> employees { get; }
        DbSet<Isalary> salaries { get; }
        DbSet<Ititle> titles { get; }
    }
}