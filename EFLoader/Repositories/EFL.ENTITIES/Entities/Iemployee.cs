﻿using System;
using System.Collections.Generic;

namespace EFL.IEntity
{
    public interface Iemployee
    {
        DateTime birth_date { get; set; }
        ICollection<Idept_emp> dept_emp { get; set; }
        ICollection<Idept_manager> dept_manager { get; set; }
        int emp_no { get; set; }
        string first_name { get; set; }
        string gender { get; set; }
        DateTime hire_date { get; set; }
        string last_name { get; set; }
        ICollection<Isalary> salaries { get; set; }
        ICollection<Ititle> titles { get; set; }
    }
}