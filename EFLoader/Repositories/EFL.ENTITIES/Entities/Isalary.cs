﻿using System;

namespace EFL.IEntity
{
    public interface Isalary
    {
        //Iemployee employee { get; set; }
        int emp_no { get; set; }
        DateTime from_date { get; set; }
        int salary1 { get; set; }
        DateTime to_date { get; set; }
    }
}