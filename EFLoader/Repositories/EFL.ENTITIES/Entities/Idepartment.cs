﻿using EFL.IEntity.Entities.Base;
using System.Collections.Generic;

namespace EFL.IEntity
{
    public interface Idepartment
    {
        ICollection<Idept_emp> dept_emp { get; set; }
        ICollection<Idept_manager> dept_manager { get; set; }
        string dept_name { get; set; }
        string dept_no { get; set; }
    }
}