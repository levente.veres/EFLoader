﻿using System;

namespace EFL.IEntity
{
    public interface Idept_manager
    {
        //Idepartment department { get; set; }
        string dept_no { get; set; }
        //Iemployee employee { get; set; }
        int emp_no { get; set; }
        DateTime from_date { get; set; }
        DateTime to_date { get; set; }
    }
}