﻿using System;

namespace EFL.IEntity
{
    public interface Ititle
    {
        //Iemployee employee { get; set; }
        int emp_no { get; set; }
        DateTime from_date { get; set; }
        string title1 { get; set; }
        DateTime? to_date { get; set; }
    }
}