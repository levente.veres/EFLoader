﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Common;
using EFL.IEntity;
using EFL.IEntity.Entities.Base;

namespace EFL.IEntity
{

    [DbConfigurationType(typeof(Db.Common.DefaultSqlEFConfiguration))]
    [NotMapped]
    public partial class EmptyContext : DbContext
    {
        public EmptyContext(DbConnection connection)

        {

        }
        [NotMapped]
        public virtual DbSet<department> departments { get; set; }
        [NotMapped]
        public virtual DbSet<dept_emp> dept_emp { get; set; }
        [NotMapped]
        public virtual DbSet<dept_manager> dept_manager { get; set; }
        [NotMapped]
        public virtual DbSet<employee> employees { get; set; }
        [NotMapped]
        public virtual DbSet<salary> salaries { get; set; }
        [NotMapped]
        public virtual DbSet<title> titles { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<department>()
        //        .Property(e => e.dept_no)
        //        .IsUnicode(false);

        //    modelBuilder.Entity<department>()
        //        .Property(e => e.dept_name)
        //        .IsUnicode(false);

        //    modelBuilder.Entity<dept_emp>()
        //        .Property(e => e.dept_no)
        //        .IsUnicode(false);

        //    modelBuilder.Entity<dept_manager>()
        //        .Property(e => e.dept_no)
        //        .IsUnicode(false);

        //    modelBuilder.Entity<employee>()
        //        .Property(e => e.first_name)
        //        .IsUnicode(false);

        //    modelBuilder.Entity<employee>()
        //        .Property(e => e.last_name)
        //        .IsUnicode(false);

        //    modelBuilder.Entity<employee>()
        //        .Property(e => e.gender)
        //        .IsUnicode(false);

        //    modelBuilder.Entity<title>()
        //        .Property(e => e.emp_title)
        //        .IsUnicode(false);
        //}
    }
}
