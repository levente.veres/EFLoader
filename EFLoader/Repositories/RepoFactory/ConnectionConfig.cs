﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.RepoFactory
{
    public static class ConnectionConfig
    {
        public static ConnectionStringSettings configMsSql = ConfigurationManager.ConnectionStrings["MsSqlConfig"];
        public static ConnectionStringSettings configMySql = ConfigurationManager.ConnectionStrings["MySqlConfig"];
        public static ConnectionStringSettings configOracle= ConfigurationManager.ConnectionStrings["OracleConfig"];
        public  static ConnectionStringSettings configSqlLite = ConfigurationManager.ConnectionStrings["SQlLiteConfig"];
    }
}
