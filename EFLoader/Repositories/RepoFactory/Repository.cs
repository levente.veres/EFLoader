﻿using EFL.MSSQL;
using EFL.MySQL;
using EFL.Oracle;
using EFL.SqlLite;
using System.Configuration;
using System.Data.Common;

namespace EFL.RepoFactory
{
    public class Repository
    {
        
        public MsSqlContext Context_MsSQL
        {
          get { return new MsSqlContext(factoryConnection(ConnectionConfig.configMsSql)); }
        }

        public MySqlContext Context_MySQL
        {
          get { return new MySqlContext(factoryConnection(ConnectionConfig.configMySql)); }
        }

        public OracleContext Context_Oracle
        {
         get { return new OracleContext(factoryConnection(ConnectionConfig.configOracle)); }
        }

        public SqlLiteContext Context_SqlLite {
            get { return new EFL.SqlLite.SqlLiteContext(factoryConnection(ConnectionConfig.configSqlLite)); }
}

        private DbConnection factoryConnection(ConnectionStringSettings config)
        {
            LogMgr.Log _log = new LogMgr.Log(LogMgr.ScopeEnum.Database);
            DbProviderFactory factory = DbProviderFactories.GetFactory(config.ProviderName);

            DbConnection connection = factory.CreateConnection();
            connection.ConnectionString = config.ConnectionString;
            _log.Info("Opening "+config.Name + " with provider " + config.ProviderName);
            connection.Open();

            _log.Info("Server status [" + connection.State + "] -> [" + connection.DataSource + "~"+ connection.ServerVersion + "]");                
            return connection;
        }
    }
}