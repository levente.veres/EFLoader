namespace EFL.MySQL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Common;
    using EFL.IEntity;
    using LogMgr;

    [DbConfigurationType(typeof(Db.Common.DefaultSqlEFConfiguration))]
    public partial class MySqlContext : DbContext
    {
        public MySqlContext(DbConnection connection)
           // : base("name=MySqlModel")
           :  base(connection, true)
        {
            this.Database.Log = s => new Log(ScopeEnum.RawMySQL).RawMYSQL(s);
        }

        public virtual DbSet<department> departments { get; set; }
        public virtual DbSet<dept_emp> dept_emp { get; set; }
        public virtual DbSet<dept_manager> dept_manager { get; set; }
        public virtual DbSet<employee> employees { get; set; }
        public virtual DbSet<salary> salaries { get; set; }
        public virtual DbSet<title> titles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<department>()
                .Property(e => e.dept_no)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.dept_name)
                .IsUnicode(false);

            modelBuilder.Entity<dept_emp>()
                .Property(e => e.dept_no)
                .IsUnicode(false);

            modelBuilder.Entity<dept_manager>()
                .Property(e => e.dept_no)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.gender)
                .IsUnicode(false);

            modelBuilder.Entity<title>()
                .Property(e => e.emp_title)
                .IsUnicode(false);
        }
    }
}
