namespace EFL.MySQL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("employees.dept_emp")]
    public partial class dept_emp
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int emp_no { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "char")]
        [StringLength(4)]
        public string dept_no { get; set; }

        [Column(TypeName = "date")]
        public DateTime from_date { get; set; }

        [Column(TypeName = "date")]
        public DateTime to_date { get; set; }

        public virtual department department { get; set; }

        public virtual employee employee { get; set; }
    }
}
