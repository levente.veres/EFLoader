namespace EFL.MySQL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("employees.departments")]
    public partial class department
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public department()
        {
            dept_emp = new HashSet<dept_emp>();
            dept_manager = new HashSet<dept_manager>();
        }

        [Key]
        [Column(TypeName = "char")]
        [StringLength(4)]
        public string dept_no { get; set; }

        [Required]
        [StringLength(40)]
        public string dept_name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dept_emp> dept_emp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dept_manager> dept_manager { get; set; }
    }
}
