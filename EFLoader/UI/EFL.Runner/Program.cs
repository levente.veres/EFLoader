﻿using EFL.Db.Common;
using EFL.LogMgr;
using EFL.TestList;
using EFL.Timer;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace EFL.Runner
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {

                if (args == null || !ValidArguments(args))
                {
                    Console.WriteLine("Select one of the switches [MYSQL, MSSQL, ORACLE, SQLLITE, ALL, HEAT]");
                }
                else
                {
                    string PARAM = args[0];

                    if (args.Length > 1)
                    {
                        PARAM_TESTRUN = Convert.ToInt16(args[1]);
                    }

                    string finalTime;
                    using (var _mainLog = new Log(ScopeEnum.Core))
                    {
                        _mainLog.Warn("Start");
                        using (var timer = new Watcher())
                        {
                            timer.Start();

                            DbConfiguration.SetConfiguration(new DefaultSqlEFConfiguration());

                            InitDatabase dbSlot = new InitDatabase();

                            HeatUpTest hul = new HeatUpTest();

                            if (PARAM == Switch.HEAT || PARAM == Switch.ALL)
                            {
                                RunHeatUpTest(dbSlot, hul);
                            }

                            if (PARAM == Switch.ALL)
                            {
                                RunOnAllDatabaseTests(dbSlot, hul);
                            }

                            if (PARAM == Switch.MSSQL)
                            {
                                RunOnMsSQLTests(dbSlot, hul);
                            }

                            if (PARAM == Switch.MYSQL)
                            {
                                RunOnMySQLTests(dbSlot, hul);
                            }

                            if (PARAM == Switch.ORACLE)
                            {
                                RunOnOracleTests(dbSlot, hul);
                            }

                            if (PARAM == Switch.SQLLITE)
                            {
                                RunOnSqlLiteTests(dbSlot, hul);
                            }

                            timer.Stop();
                            finalTime = timer.getElapsedFormated;
                            // Console.WriteLine("Core Runtime: ");
                        }
                        _mainLog.Warn("Full runtime: " + finalTime);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error found!!! \n\r_________________  \n\r" + ex.ToString());            
            }
        }

        private static void RunHeatUpTest(InitDatabase dbSlot, HeatUpTest hul)
        {
            hul.HeatMsSQL(dbSlot.MsSQL);
            hul.HeatMySQL(dbSlot.MySQL);
            hul.HeatOracle(dbSlot.Oracle);
            hul.HeatSqlLite(dbSlot.SqlLite);
        }

        private static void RunOnSqlLiteTests(InitDatabase dbSlot, HeatUpTest hul)
        {
            var runSlot = dbSlot.SqlLite;
            hul.HeatSqlLite(runSlot);
            RunTestSqlLite runTest = new RunTestSqlLite(runSlot);
            switch (PARAM_TESTRUN)
            {
                case 1:
                    runTest.RunSmallAll();
                    break;
                case 2:
                    runTest.RunAll();
                    break;
                default:
                    runTest.RunSelects();
                    break;
            }
        }

        private static void RunOnOracleTests(InitDatabase dbSlot, HeatUpTest hul)
        {
            var runSlot = dbSlot.Oracle;
            hul.HeatOracle(runSlot);
            RunTestOracle runTest = new RunTestOracle(runSlot);

            switch (PARAM_TESTRUN)
            {
                case 1:
                    runTest.RunSmallAll();
                    break;
                case 2:
                    runTest.RunAll();
                    break;
                default:
                    runTest.RunSelects();
                    break;
            }
        }

        private static void RunOnMySQLTests(InitDatabase dbSlot, HeatUpTest hul)
        {
            var runSlot = dbSlot.MySQL;
            hul.HeatMySQL(runSlot);
            RunTestMySQL runTest = new RunTestMySQL(runSlot);

            switch (PARAM_TESTRUN)
            {
                case 1:
                    runTest.RunSmallAll();
                    break;
                case 2: runTest.RunAll();
                    break;
                default:
                    runTest.RunSelects();
                    break;
            }

        }

        private static void RunOnMsSQLTests(InitDatabase dbSlot, HeatUpTest hul)
        {
            var runSlot = dbSlot.MsSQL;
            hul.HeatMsSQL(runSlot);
            RunTestMsSQL runTest = new RunTestMsSQL(runSlot);
            switch (PARAM_TESTRUN)
            {
                case 1:
                    runTest.RunSmallAll();
                    break;
                case 2:
                    runTest.RunAll();
                    break;
                default:
                    runTest.RunSelects();
                    break;
            }
        }

        private static void RunOnAllDatabaseTests(InitDatabase dbSlot, HeatUpTest hul)
        {
            var runSlot = dbSlot.MsSQL;
            hul.HeatMsSQL(runSlot);
            RunTestMsSQL runTestMsSQL = new RunTestMsSQL(runSlot);
            switch (PARAM_TESTRUN)
            {
                case 1:
                    runTestMsSQL.RunSmallAll();
                    break;
                case 2:
                    runTestMsSQL.RunAll();
                    break;
                default:
                    runTestMsSQL.RunSelects();
                    break;
            }

            var runSlotMySQL = dbSlot.MySQL;
            hul.HeatMySQL(runSlotMySQL);
            RunTestMySQL runTestMysql = new RunTestMySQL(runSlotMySQL);
            switch (PARAM_TESTRUN)
            {
                case 1:
                    runTestMysql.RunSmallAll();
                    break;
                case 2:
                    runTestMysql.RunAll();
                    break;
                default:
                    runTestMysql.RunSelects();
                    break;
            }

            var runSlotOracle = dbSlot.Oracle;
            hul.HeatOracle(runSlotOracle);
            RunTestOracle runTestOracle = new RunTestOracle(runSlotOracle);
            switch (PARAM_TESTRUN)
            {
                case 1:
                    runTestOracle.RunSmallAll();
                    break;
                case 2:
                    runTestOracle.RunAll();
                    break;
                default:
                    runTestOracle.RunSelects();
                    break;
            }

            var runSlotSqlLite = dbSlot.SqlLite;
            hul.HeatSqlLite(runSlotSqlLite);
            RunTestSqlLite runTestSqlLite = new RunTestSqlLite(runSlotSqlLite);
            switch (PARAM_TESTRUN)
            {
                case 1:
                    runTestSqlLite.RunSmallAll();
                    break;
                case 2:
                    runTestSqlLite.RunAll();
                    break;
                default:
                    runTestSqlLite.RunSelects();
                    break;
            }
        }

        private static List<string> paramList = new List<string>() { Switch.ALL, Switch.HEAT, Switch.MSSQL, Switch.MYSQL, Switch.ORACLE, Switch.SQLLITE };
        private static int PARAM_TESTRUN = 0;
        private static bool ValidArguments(string[] args)
        {
            if (args.Length > 1)
            {
                if (paramList.Contains(args[0]))
                {
                    return true;
                }
            }

            return false;
        }
    }
}