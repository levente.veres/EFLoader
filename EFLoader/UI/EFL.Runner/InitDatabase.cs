﻿using EFL.LogMgr;
using EFL.MSSQL;
using EFL.MySQL;
using EFL.Oracle;
using EFL.RepoFactory;
using EFL.SqlLite;

namespace EFL.Runner
{
    public  class InitDatabase
    {
        public InitDatabase()
        {
            using (var _log = new Log(ScopeEnum.Database)) {
                _log.Info("Init database servers");
                 _msSQL = new Repository().Context_MsSQL;

                _mySQL = new Repository().Context_MySQL;

                _Oracle = new Repository().Context_Oracle;

                _sqlLite = new Repository().Context_SqlLite;
            }
        }

        private MsSqlContext _msSQL;
        public MsSqlContext MsSQL { get { return _msSQL; }  }

        private MySqlContext _mySQL;
        public MySqlContext MySQL { get { return _mySQL; } }

        private OracleContext _Oracle;
        public OracleContext Oracle { get { return _Oracle; } }

        private SqlLiteContext _sqlLite;
        public SqlLiteContext SqlLite { get { return _sqlLite; } }


    }
}
