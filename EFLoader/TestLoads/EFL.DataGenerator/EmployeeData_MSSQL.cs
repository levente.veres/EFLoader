﻿using System;
using System.Collections.Generic;
using EFL.MSSQL;


namespace EFL.DataGenerator
{
    public class EmployeeData_MSSQL
    {

        public int EMP_NO_INDEX = EmployeeConfig.EMP_NO_INDEX;
        public int EMP_NEW_SALARY = EmployeeConfig.EMP_NEW_SALARY;
        public int EMP_UPDATED_SALARY = EmployeeConfig.EMP_UPDATED_SALARY;
        public string DEP_NO_USED= EmployeeConfig.DEP_NO_USED;
        public string EMP_TITLE = EmployeeConfig.EMP_TITLE;
        public string EMP_FIRSTNAME_IDX = EmployeeConfig.EMP_FIRSTNAME_IDX;
        public string EMP_LASTNAME_IDX = EmployeeConfig.EMP_LASTNAME_IDX;
        public DateTime USE_FROM_DT = EmployeeConfig.USE_FROM_DT;
        public DateTime USE_TO_DT = EmployeeConfig.USE_TO_DT;

        #region Generator_Data
        
        public List<employee> GenerateEmployeeWithDepartmentTitleSalary(int generateSize)
        {
            var empList = new List<employee>();

            var newTitle = new title() { emp_title = EMP_TITLE, from_date = USE_FROM_DT, to_date = USE_TO_DT };
            var newSalary = new salary() { emp_salary = EMP_NEW_SALARY, from_date = USE_FROM_DT, to_date = USE_TO_DT };

            for (int i = 0; i < generateSize; i++)
            {
                var new_emp_no = EMP_NO_INDEX + generateSize + i;
                var new_gender = (i%2 == 0) ? "M" : "F";

                newSalary.emp_no = new_emp_no;
                newTitle.emp_no = new_emp_no;

                var new_dep_emp = new dept_emp()
                {
                    emp_no = new_emp_no,
                    dept_no = DEP_NO_USED,
                    from_date = USE_FROM_DT,
                    to_date = USE_TO_DT
                };

                var new_employee = new employee()
                {
                    emp_no = new_emp_no,
                    birth_date = DateTime.Today,
                    hire_date = DateTime.Today,
                    gender = new_gender,
                    first_name = EMP_FIRSTNAME_IDX + new_emp_no,
                    last_name = EMP_LASTNAME_IDX + new_gender
                };

                new_employee.salaries.Add(newSalary);
                new_employee.titles.Add(newTitle);
                new_employee.dept_emp.Add(new_dep_emp);

                empList.Add(new_employee);
            }
            
            return empList;
        }


        public salary GetOneSalary()
        {
           return  new salary() { emp_salary = EMP_NEW_SALARY+999, from_date = USE_FROM_DT, to_date = USE_TO_DT };            
        }

        #endregion


    }
}
