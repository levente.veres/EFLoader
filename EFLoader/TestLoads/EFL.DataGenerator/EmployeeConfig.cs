﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.DataGenerator
{
    public static class EmployeeConfig
    {
        public static int EMP_NO_INDEX = 2000000;
        public static int EMP_NEW_SALARY = 55000;
        public static int EMP_UPDATED_SALARY = 75000;
        public static string DEP_NO_USED = "d004";
        public static string EMP_TITLE = "TEST_EMPLOYEE";
        public static string EMP_FIRSTNAME_IDX = "FN_";
        public static string EMP_LASTNAME_IDX = "LN_";
        public static DateTime USE_FROM_DT = DateTime.Today;
        public static DateTime USE_TO_DT = DateTime.Today;
    }
}
