﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.TestList
{
    public static class TestMessage
    {
        public static string TEST_01_INSERT_1000 = "TEST_01_INSERT_1000";
        public static string TEST_02_INSERT_10000 = "TEST_02_INSERT_10000";
        public static string TEST_03_INSERT_100000 = "TEST_03_INSERT_100000";
        public static string TEST_04_SELECT_1000 = "TEST_04_SELECT_1000";
        public static string TEST_05_SELECT_10000 = "TEST_05_SELECT_10000";
        public static string TEST_06_SELECT_100000 = "TEST_06_SELECT_100000";
        public static string TEST_07_UPDATE_1000 = "TEST_07_UPDATE_1000";
        public static string TEST_08_UPDATE_10000 = "TEST_08_UPDATE_10000";
        public static string TEST_09_UPDATE_100000 = "TEST_09_UPDATE_100000";
        public static string TEST_10_DELETE_1000 = "TEST_10_DELETE_1000";
        public static string TEST_11_DELETE_10000 = "TEST_11_DELETE_10000";
        public static string TEST_12_DELETE_100000 = "TEST_12_DELETE_100000";
        public static string TEST_13_UPSERT_1000 = "TEST_13_UPSERT_1000";
        public static string TEST_14_UPSERT_10000 = "TEST_14_UPSERT_10000";
        public static string TEST_15_UPSERT_100000 = "TEST_15_UPSERT_100000";

        public static string TEST_16_SALARY_SUM_10000 = "TEST_16_SALARY_SUM_10000";
        public static string TEST_17_AVRG_SALARY_10000 = "TEST_17_AVRG_SALARY_10000";
        public static string TEST_18_SALARY_SUM_DEPARTMENT_10000 = "TEST_18_SALARY_SUM_DEPARTMENT_10000";

        public static string TEST_19_WHERE_EMPLOYEE = "TEST_19_WHERE_EMPLOYEE";
        public static string TEST_20_WHERE_SALARY = "TEST_20_WHERE_SALARY";

        public static string HEAT_01_DEPARTMENTS = "HEAT_01_DEPARTMENTS";
        public static string HEAT_02_EMP_SALARY = "HEAT_02_EMP_SALARY";

    }
}
