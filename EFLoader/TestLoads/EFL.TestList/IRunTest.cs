﻿namespace EFL.TestList
{
    internal interface IRunTest
    {
        void TEST_01_INSERT_1000();

        void TEST_02_INSERT_10000();

        void TEST_03_INSERT_100000();

        void TEST_04_SELECT_1000();

        void TEST_05_SELECT_10000();

        void TEST_06_SELECT_100000();

        void TEST_07_UPDATE_1000();

        void TEST_08_UPDATE_10000();

        void TEST_09_UPDATE_100000();

        void TEST_10_DELETE_1000();

        void TEST_11_DELETE_10000();

        void TEST_12_DELETE_100000();

        void TEST_13_UPSERT_1000();

        void TEST_14_UPSERT_10000();

        void TEST_15_UPSERT_100000();

        void TEST_16_SALARY_SUM_10000();

        void TEST_17_AVRG_SALARY_10000();

        void TEST_18_SALARY_SUM_DEPARTMENT_10000();

        void TEST_19_WHERE_EMPLOYEE();

        void TEST_20_WHERE_SALARY();
    }
}