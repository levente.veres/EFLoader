﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.TestList
{
    static class Configs
    {
        public static int RUN_1_000 = 1000;
        public static int RUN_10_000 = 10000;
        public static int RUN_100_000 = 100000;

    }
}
