﻿using EFL.DataGenerator;
using EFL.LogMgr;
using EFL.MSSQL;
using System.Data.Entity;
using System.Linq;

namespace EFL.TestList
{
    public class RunTestMsSQL : IRunTest
    {
        private MsSqlContext model;
        private ScopeEnum scopeNow = ScopeEnum.MsSQL;
        private EmployeeData_MSSQL EmpData = new EmployeeData_MSSQL();

        public RunTestMsSQL(MsSqlContext dbModel)
        {
            model = dbModel;
        }

        public void RunAll()
        {
            TEST_01_INSERT_1000();
            TEST_02_INSERT_10000();
            TEST_03_INSERT_100000();
            TEST_04_SELECT_1000();
            TEST_05_SELECT_10000();
            TEST_06_SELECT_100000();
            TEST_07_UPDATE_1000();
            TEST_08_UPDATE_10000();
            TEST_09_UPDATE_100000();
            TEST_10_DELETE_1000();
            TEST_11_DELETE_10000();
            TEST_12_DELETE_100000();
            TEST_13_UPSERT_1000();
            TEST_14_UPSERT_10000();
            TEST_15_UPSERT_100000();
            TEST_16_SALARY_SUM_10000();
            TEST_17_AVRG_SALARY_10000();
            TEST_18_SALARY_SUM_DEPARTMENT_10000();
            TEST_19_WHERE_EMPLOYEE();
            TEST_20_WHERE_SALARY();
        }

        public void RunSmallAll()
        {
            TEST_01_INSERT_1000();
            TEST_04_SELECT_1000();
            TEST_07_UPDATE_1000();
            TEST_10_DELETE_1000();
            TEST_13_UPSERT_1000();
            TEST_16_SALARY_SUM_10000();
            TEST_17_AVRG_SALARY_10000();
            TEST_18_SALARY_SUM_DEPARTMENT_10000();
            TEST_19_WHERE_EMPLOYEE();
            TEST_20_WHERE_SALARY();
        }

        public void RunSelects()
        {
            TEST_04_SELECT_1000();
            TEST_05_SELECT_10000();
            TEST_06_SELECT_100000();
        }

        public void TEST_01_INSERT_1000()
        {
            using (var _log = new Log(scopeNow))
            {
                var empList = EmpData.GenerateEmployeeWithDepartmentTitleSalary(Configs.RUN_1_000);
                model.employees.AddRange(empList);

                model.SaveChanges();
                _log.Info(TestMessage.TEST_01_INSERT_1000);
            }
        }

        public void TEST_02_INSERT_10000()
        {
            using (var _log = new Log(scopeNow))
            {
                model.employees.AddRange(EmpData.GenerateEmployeeWithDepartmentTitleSalary(Configs.RUN_10_000));
                model.SaveChanges();
                _log.Info(TestMessage.TEST_02_INSERT_10000);
            }
        }

        public void TEST_03_INSERT_100000()
        {
            using (var _log = new Log(scopeNow))
            {
                model.employees.AddRange(EmpData.GenerateEmployeeWithDepartmentTitleSalary(Configs.RUN_100_000));
                model.SaveChanges();
                _log.Info(TestMessage.TEST_03_INSERT_100000);
            }
        }

        public void TEST_04_SELECT_1000()
        {
            using (var _log = new Log(scopeNow))
            {
                var items = model.employees.Take(Configs.RUN_1_000).ToList();
                _log.Info(TestMessage.TEST_04_SELECT_1000);
            }
        }

        public void TEST_05_SELECT_10000()
        {
            using (var _log = new Log(scopeNow))
            {
                var items = model.employees.Take(Configs.RUN_10_000).ToList();
                _log.Info(TestMessage.TEST_05_SELECT_10000);
            }
        }

        public void TEST_06_SELECT_100000()
        {
            using (var _log = new Log(scopeNow))
            {
                var items = model.employees.Take(Configs.RUN_100_000).ToList();
                _log.Info(TestMessage.TEST_06_SELECT_100000);
            }
        }

        public void TEST_07_UPDATE_1000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX;
            int highIndex = lowIndex + Configs.RUN_1_000;

            using (var _log = new Log(scopeNow))
            {
                model.salaries
                        .Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex)
                        .ToList()
                        .ForEach(x => x.emp_salary = EmpData.EMP_UPDATED_SALARY);
                model.SaveChanges();

                _log.Info(TestMessage.TEST_07_UPDATE_1000);
            }
        }

        public void TEST_08_UPDATE_10000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX;
            int highIndex = lowIndex + Configs.RUN_10_000;

            using (var _log = new Log(scopeNow))
            {
                model.salaries
                        .Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex)
                        .ToList()
                        .ForEach(x => x.emp_salary = EmpData.EMP_UPDATED_SALARY);
                model.SaveChanges();

                _log.Info(TestMessage.TEST_08_UPDATE_10000);
            }
        }

        public void TEST_09_UPDATE_100000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX;
            int highIndex = lowIndex + Configs.RUN_100_000;

            using (var _log = new Log(scopeNow))
            {
                model.salaries
                        .Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex)
                        .ToList()
                        .ForEach(x => x.emp_salary = EmpData.EMP_UPDATED_SALARY);
                model.SaveChanges();

                _log.Info(TestMessage.TEST_09_UPDATE_100000);
            }
        }

        public void TEST_10_DELETE_1000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX + Configs.RUN_1_000;
            int highIndex = lowIndex + Configs.RUN_1_000;

            using (var _log = new Log(scopeNow))
            {
                model.salaries.RemoveRange(model.salaries.Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex));

                model.SaveChanges();

                _log.Info(TestMessage.TEST_10_DELETE_1000);
            }
        }

        public void TEST_11_DELETE_10000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX + Configs.RUN_10_000;
            int highIndex = lowIndex + Configs.RUN_10_000;

            using (var _log = new Log(scopeNow))
            {
                model.salaries.RemoveRange(model.salaries.Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex));

                model.SaveChanges();

                _log.Info(TestMessage.TEST_11_DELETE_10000);
            }
        }

        public void TEST_12_DELETE_100000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX + Configs.RUN_100_000;
            int highIndex = lowIndex + Configs.RUN_100_000;

            using (var _log = new Log(scopeNow))
            {
                model.salaries.RemoveRange(model.salaries.Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex));

                model.SaveChanges();

                _log.Info(TestMessage.TEST_12_DELETE_100000);
            }
        }

        public void TEST_13_UPSERT_1000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX + Configs.RUN_1_000;
            int highIndex = lowIndex + Configs.RUN_1_000;

            using (var _log = new Log(scopeNow))
            {
                var empList = model.employees.Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex).ToList();
                foreach (var item in empList)
                {
                    var salary = EmpData.GetOneSalary();
                    item.first_name = item.first_name + "_UPD";
                    salary.emp_no = item.emp_no;
                    item.salaries.Add(salary);
                }

                model.SaveChanges();

                _log.Info(TestMessage.TEST_13_UPSERT_1000);
            }
        }

        public void TEST_14_UPSERT_10000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX + Configs.RUN_10_000;
            int highIndex = lowIndex + Configs.RUN_10_000;

            using (var _log = new Log(scopeNow))
            {
                var empList = model.employees.Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex).ToList();
                foreach (var item in empList)
                {
                    var salary = EmpData.GetOneSalary();
                    item.first_name = item.first_name + "_UPD";
                    salary.emp_no = item.emp_no;
                    item.salaries.Add(salary);
                }

                model.SaveChanges();

                _log.Info(TestMessage.TEST_14_UPSERT_10000);
            }
        }

        public void TEST_15_UPSERT_100000()
        {
            int lowIndex = EmpData.EMP_NO_INDEX + Configs.RUN_100_000;
            int highIndex = lowIndex + Configs.RUN_100_000;

            using (var _log = new Log(scopeNow))
            {
                var empList = model.employees.Where(x => x.emp_no > lowIndex && x.emp_no <= highIndex).ToList();
                foreach (var item in empList)
                {
                    var salary = EmpData.GetOneSalary();
                    item.first_name = item.first_name + "_UPD";
                    salary.emp_no = item.emp_no;
                    item.salaries.Add(salary);
                }

                model.SaveChanges();

                _log.Info(TestMessage.TEST_15_UPSERT_100000);
            }
        }

        public void TEST_16_SALARY_SUM_10000()
        {
            using (var _log = new Log(scopeNow))
            {
                var empNo = model.employees.AsNoTracking().Take(Configs.RUN_10_000).Select(x => x.emp_no).ToList();
                long countedEmp = model.salaries.AsNoTracking()
                    .Where(x => empNo.Contains(x.emp_no) && x.emp_salary > 0).Select(x => x.emp_salary / 1000)
                    .AsEnumerable().Sum();
                _log.Info(TestMessage.TEST_16_SALARY_SUM_10000);
                _log.Debug(TestMessage.TEST_16_SALARY_SUM_10000 + "|" + countedEmp + "x1000");
            }
        }

        public void TEST_17_AVRG_SALARY_10000()
        {
            using (var _log = new Log(scopeNow))
            {
                var empNo = model.employees.AsNoTracking().Take(Configs.RUN_10_000).Select(x => x.emp_no).ToList();
                var countedEmp = model.salaries.AsNoTracking().Where(x => empNo.Contains(x.emp_no)).Average(x => x.emp_salary);
                _log.Info(TestMessage.TEST_17_AVRG_SALARY_10000);
                _log.Debug(TestMessage.TEST_17_AVRG_SALARY_10000 + "|" + countedEmp);
            }
        }

        public void TEST_18_SALARY_SUM_DEPARTMENT_10000()
        {
            using (var _log = new Log(scopeNow))
            {
                var empOfDepartemnt = model.dept_emp.AsNoTracking().Where(x => x.dept_no == EmpData.DEP_NO_USED).Take(Configs.RUN_10_000).Select(x => x.emp_no).ToArray();
                var countedEmp = model.salaries.AsNoTracking().Where(x => empOfDepartemnt.Contains(x.emp_no)).Sum(x => x.emp_salary / 1000);
                _log.Info(TestMessage.TEST_18_SALARY_SUM_DEPARTMENT_10000);
                _log.Debug(TestMessage.TEST_18_SALARY_SUM_DEPARTMENT_10000 + "|" + countedEmp + "x1000");
            }
        }

        public void TEST_19_WHERE_EMPLOYEE()
        {
            using (var _log = new Log(scopeNow))
            {
                var countedEmp = model.employees.AsNoTracking().Where(x => x.first_name.Contains(EmpData.EMP_FIRSTNAME_IDX)).Count();
                _log.Info(TestMessage.TEST_19_WHERE_EMPLOYEE);
                _log.Debug(TestMessage.TEST_19_WHERE_EMPLOYEE + "|" + countedEmp);
            }
        }

        public void TEST_20_WHERE_SALARY()
        {
            using (var _log = new Log(scopeNow))
            {
                var countedSalary = model.salaries.AsNoTracking().Where(x => x.emp_salary > 50000).Count();
                _log.Info(TestMessage.TEST_20_WHERE_SALARY);
                _log.Debug(TestMessage.TEST_20_WHERE_SALARY + "|" + countedSalary);
            }
        }
    }
}