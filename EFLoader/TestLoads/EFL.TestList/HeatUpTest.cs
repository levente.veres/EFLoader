﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFL.MySQL;
using EFL.MSSQL;
using EFL.Oracle;
using EFL.SqlLite;
using EFL.Db.Common;
using EFL.LogMgr;
using System.Diagnostics;

namespace EFL.TestList
{
    public class HeatUpTest
    {
        public int TAKE_ITEMS = 20;

        public void HeatMsSQL(MsSqlContext model)
        {
            
            using (var _log = new Log(ScopeEnum.MsSQL)) {

                foreach (var item in model.departments.Take(TAKE_ITEMS).ToList())
                {
                    Trace.WriteLine(item.dept_no + "- " + item.dept_name);
                    var resp1 = item.dept_no + "- " + item.dept_name;
                }
                _log.Info(TestMessage.HEAT_01_DEPARTMENTS);

                foreach (var item in model.employees.Take(TAKE_ITEMS).ToList())

                {
                    Trace.WriteLine(item.first_name + "- " + item.salaries.FirstOrDefault().emp_salary);
                    var resp2 = item.first_name + "- " + item.salaries.FirstOrDefault().emp_salary;
                }
                _log.Info(TestMessage.HEAT_02_EMP_SALARY);
            }

        }

        public void HeatMySQL(MySqlContext model)
        {
            using (var _log = new Log(ScopeEnum.MySQL))
            {

                foreach (var item in model.departments.Take(TAKE_ITEMS).ToList())
                {
                    Trace.WriteLine(item.dept_no + "- " + item.dept_name);
                    var resp1 = item.dept_no + "- " + item.dept_name;
                }
                _log.Info(TestMessage.HEAT_01_DEPARTMENTS);

                foreach (var item in model.employees.Take(TAKE_ITEMS).ToList())

                {
                    Trace.WriteLine(item.first_name + "- " + item.salaries.FirstOrDefault().emp_salary);
                    var resp2 = item.first_name + "- " + item.salaries.FirstOrDefault().emp_salary;
                }
                _log.Info(TestMessage.HEAT_02_EMP_SALARY);
            }
        }


        public void HeatOracle(OracleContext model)
        {
            using (var _log = new Log(ScopeEnum.Oracle))
            {

                foreach (var item in model.departments.Take(TAKE_ITEMS).ToList())
                {
                    Trace.WriteLine(item.dept_no + "- " + item.dept_name);
                    var resp1 = item.dept_no + "- " + item.dept_name;
                }
                _log.Info(TestMessage.HEAT_01_DEPARTMENTS);

                foreach (var item in model.employees.Take(TAKE_ITEMS).ToList())

                {
                    Trace.WriteLine(item.first_name + "- " + item.salaries.FirstOrDefault().emp_salary);
                    var resp2 = item.first_name + "- " + item.salaries.FirstOrDefault().emp_salary;
                }
                _log.Info(TestMessage.HEAT_02_EMP_SALARY);
            }
        }
        public void HeatSqlLite(SqlLiteContext model)
        {
            using (var _log = new Log(ScopeEnum.SqlLite))
            {

                foreach (var item in model.departments.Take(TAKE_ITEMS).ToList())
                {
                    Trace.WriteLine(item.dept_no + "- " + item.dept_name);
                    var resp1 = item.dept_no + "- " + item.dept_name;
                }
                _log.Info(TestMessage.HEAT_01_DEPARTMENTS);

                foreach (var item in model.employees.Take(TAKE_ITEMS).ToList())

                {
                    Trace.WriteLine(item.first_name + "- " + item.salaries.FirstOrDefault().emp_salary);
                    var resp2 = item.first_name + "- " + item.salaries.FirstOrDefault().emp_salary;
                }
                _log.Info(TestMessage.HEAT_02_EMP_SALARY);
            }
        }

    }
}
