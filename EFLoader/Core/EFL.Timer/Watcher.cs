﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.Timer
{
    public class Watcher : IDisposable
    {
        Stopwatch sw;
        double elapsedTime;
        TimeSpan elapsed; 

        public void Start()
        {
            this.sw = Stopwatch.StartNew();
        }

        public void Stop()
        {
            
            elapsed = sw.Elapsed;
            elapsedTime = elapsed.TotalMilliseconds;
            this.sw.Restart();
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
            sw.Stop();
        }

        public double getElapsedMilliseconds
        {
            get { return elapsedTime; }
        }
        public TimeSpan getElapsed
        {
            get { return elapsed; }
        }

        public string getElapsedFormated
        {
            get { return elapsed.ToString(@"hh\:mm\:ss\.fff"); }
        }


    }
}
