﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Diagnostics;
using NLog.LayoutRenderers;
using NLog.Config;

namespace EFL.LogMgr
{
    //[LayoutRenderer("elapsedtime")]
    //[ThreadAgnostic]
    //public class ElapsedTimeLayoutRenderer : LayoutRenderer
    //{
    //    Stopwatch sw;

    //    public ElapsedTimeLayoutRenderer()
    //    {
    //        this.sw = Stopwatch.StartNew();
    //    }

    //    protected override void Append(StringBuilder builder, LogEventInfo logEvent)
    //    {
    //        var diffms = this.sw.ElapsedMilliseconds.ToString();
    //        builder.Append(diffms);
    //        this.sw.Restart();
    //    }
    //}

    public class Log : IDisposable
    {
        private Logger _logger = LogManager.CreateNullLogger();
        private Logger _loggerJsonMsSQL = LogManager.CreateNullLogger();
        private Logger _loggerJsonMySQL = LogManager.CreateNullLogger();
        private Logger _loggerJsonOracle = LogManager.CreateNullLogger();        


        public Log (ScopeEnum scopeName)
        {
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("elapsedtime", typeof(ElapsedTimeLayoutRenderer));
            ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("totalelapsedtime", typeof(TotalElapsedTimeLayoutRenderer));

            _loggerJsonMsSQL = LogManager.GetLogger("JsonMsSQL");
        _loggerJsonMySQL = LogManager.GetLogger("JsonMySQL");
        _loggerJsonOracle = LogManager.GetLogger("JsonOracle");

        _logger = LogManager.GetLogger(scopeName.ToString());
        }

        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Info(string message)
        {

            _logger.Info(message);
        }

        public void Trace(string message)
        {

            _logger.Trace(message);
        }

        public void Warn(string message)
        {

            _logger.Warn(message);
        }

        public void Error(string message)
        {

            _logger.Error(message);
        }

        public void JsonSave(string message, ScopeEnum scope)
        {
            switch (scope)
            {
                case ScopeEnum.MsSQL:
                    _loggerJsonMsSQL.Info(message);
                    break;
                case ScopeEnum.MySQL:
                    _loggerJsonMySQL.Info(message);
                    break;
                case ScopeEnum.Oracle:
                    _loggerJsonOracle.Info(message);
                    break;
                default:
                    break;
            }
        }

        public void Dispose()
        {
            _logger.Factory.Dispose();
        }
        public void RawMSSQL(string message)
        {
            var _loggerRaw = LogManager.GetLogger(ScopeEnum.RawMsSQL.ToString());
            _loggerRaw.Trace(message);
        }


        public void RawMYSQL(string message)
        {
            var _loggerRaw = LogManager.GetLogger(ScopeEnum.RawMySQL.ToString());
            _loggerRaw.Trace(message);
        }

        public void RawORACLE(string message)
        {
            var _loggerRaw = LogManager.GetLogger(ScopeEnum.RawOracle.ToString());
            _loggerRaw.Trace(message);
        }

        public void RawSQLLITE(string message)
        {
            var _loggerRaw = LogManager.GetLogger(ScopeEnum.RawSqlLite.ToString());
            _loggerRaw.Trace(message);
        }

        //public Logger Out { get { return _logger; }  }


    }
}
