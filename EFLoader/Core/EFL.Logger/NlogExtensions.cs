﻿using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.LogMgr
{
    [LayoutRenderer("elapsedtime")]
    [ThreadAgnostic]
    public class ElapsedTimeLayoutRenderer : LayoutRenderer
    {
        Stopwatch sw;

        public ElapsedTimeLayoutRenderer()
        {
            this.sw = Stopwatch.StartNew();
        }

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var diffms = this.sw.ElapsedMilliseconds.ToString();
            builder.Append(diffms);
            this.sw.Restart();
        }
    }

    [LayoutRenderer("totalelapsedtime")]
    [ThreadAgnostic]
    public class TotalElapsedTimeLayoutRenderer : LayoutRenderer
    {
        Stopwatch sw;

        public TotalElapsedTimeLayoutRenderer()
        {
            this.sw = Stopwatch.StartNew();
        }

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var diffms = this.sw.Elapsed.ToString();
            builder.Append(diffms);
            this.sw.Restart();
        }
    }

}
