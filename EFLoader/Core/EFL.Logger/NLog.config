<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.nlog-project.org/schemas/NLog.xsd NLog.xsd"
      autoReload="true"
      throwExceptions="true"
      internalLogLevel="Off" internalLogFile="D:\logs\EFL\log\nlog-internal.log">

  <variable name="logDirectory" value="D:/logs/EFL/${shortdate}/PID_${processid}" />

  <!-- optional, add some variabeles
  https://github.com/nlog/NLog/wiki/Configuration-file#variables
  -->

  <!--
  See https://github.com/nlog/nlog/wiki/Configuration-file
  for information on customizing logging rules and outputs.
   -->
  <targets>

    <!--
    add your targets here
    See https://github.com/nlog/NLog/wiki/Targets for possible targets.
    See https://github.com/nlog/NLog/wiki/Layout-Renderers for the possible layout renderers.
    -->

    <!--
    Write events to a file with the date in the filename.
    <target xsi:type="File" name="f" fileName="${basedir}/logs/${shortdate}.log"
            layout="${longdate} ${uppercase:${level}} ${message}" />
    -->



    <target name="console" xsi:type="ColoredConsole" layout="${longdate} [${whenEmpty:whenEmpty=${threadid}:inner=${threadname}}] [${processid}] ${level} ${pad:padding=10:inner=${logger:uppercase=true}} ${pad:padding=15:inner=${elapsedtime} ms}     ${message}">
      <highlight-row condition="level == LogLevel.Debug" foregroundColor="Blue" />
      <highlight-row condition="level == LogLevel.Error" foregroundColor="Red" />
      <highlight-row condition="level == LogLevel.Warn" foregroundColor="Yellow" />      
      <highlight-row condition="level == LogLevel.Info" foregroundColor="Green"  backgroundColor="Black" />
      <!-- repeated -->
    <highlight-word backgroundColor="DarkGray" foregroundColor="DarkMagenta" ignoreCase="false" regex="MSSQL" text="MSSQL" wholeWords="true" compileRegex="false"/>
      <highlight-word backgroundColor="DarkGray" foregroundColor="Blue" ignoreCase="false" regex="MYSQL" text="MYSQL" wholeWords="true" compileRegex="false"/>
      <highlight-word backgroundColor="DarkGray" foregroundColor="Red" ignoreCase="false" regex="ORACLE" text="ORACLE" wholeWords="true" compileRegex="false"/>
      <highlight-word backgroundColor="DarkGray" foregroundColor="DarkBlue" ignoreCase="false" regex="SQLLITE" text="SQLLITE" wholeWords="true" compileRegex="false"/>
      <highlight-word backgroundColor="DarkBlue" foregroundColor="White" ignoreCase="false" regex="PROCESS" text="PROCESS" wholeWords="true" compileRegex="false"/>
      <!-- repeated -->
      <highlight-row condition="level == LogLevel.Fatal" foregroundColor="Red" backgroundColor="White" />
      
      
    </target>
    
    <target name="jsonFile" xsi:type="File" fileName="${logDirectory}/Json/${logger}_${longdate}.json">
      <layout xsi:type="JsonLayout">
        <attribute name="time" layout="${longdate}" />
        <attribute name ="elapsedtime" layout="${elapsedtime}" />
        <attribute name="level" layout="${level:upperCase=true}" />
        <attribute name="message" layout="${message} ${processtime} " />
      </layout>
    </target>

    <!--<target name="logfile" xsi:type="File" fileName="${logDirectory}/logfile_all.log" />-->
    <target name="logfileSplit" xsi:type="File" layout="${level:upperCase=true}|${longdate}|${machinename}|${logger}|${message}|${elapsedtime}" fileName="${logDirectory}/logfileSplit.log" />
    <target name="consoleLogFile" xsi:type="File"  fileName="${logDirectory}/console.log" layout="${longdate} [${whenEmpty:whenEmpty=${threadid}:inner=${threadname}}] [${processid}] ${level} ${pad:padding=10:inner=${logger:uppercase=true}} ${pad:padding=15:inner=${elapsedtime} ms} ${message}" />
    <target name="viewer" xsi:type="NLogViewer" layout="${level:upperCase=true}|${longdate}|${machinename}|${logger}|${message}|${elapsedtime}|${gc}" address="udp://127.0.0.1:9999" />

    <target name="DbLog" xsi:type="File"  layout="${longdate}:${machinename}:${logger}:${message}:${elapsedtime}" fileName="${logDirectory}/Debug/${logger}_plain.log" />

    <target name="logfilePerformance" xsi:type="File"
             layout="${level:upperCase=true}|${longdate}|${machinename}|${logger}|${message}|${elapsedtime}|${gc}"
             fileName="${logDirectory}/Performance/${logger}.log" />

    <target name="logfileAllPerformance" xsi:type="File"
         layout="${level:upperCase=true}|${longdate}|${machinename}|${logger}|${message}|${elapsedtime}|${gc}"
         fileName="${logDirectory}/Performance/reportALL.log" />

    <target name="logfilePerformanceProcess" xsi:type="File"
              layout="${level:upperCase=true}|${longdate}|${machinename}|${logger}|${message}|${elapsedtime}"
              fileName="${logDirectory}/Performance/process.log" />

    <target name="logfileCore" xsi:type="File"
           layout="${level:upperCase=true}|${longdate}|${machinename}|${logger}|${message}|${elapsedtime}|${processinfo:property=WorkingSet64}"
           fileName="${logDirectory}/Core/process_time.log" />

    <target name="logfileDatabase" xsi:type="File"
       layout="${level:upperCase=true}|${longdate}|${machinename}|${logger}|${message}|${elapsedtime}|${processinfo:property=WorkingSet64}"
       fileName="${logDirectory}/database.log" />

    <target name="rawLogger" xsi:type="File" layout="${message}" fileName="${logDirectory}/${logger}_raw_sql.log" />

    <target name="jsonFileCore" xsi:type="File" fileName="${logDirectory}/Json/app_run.json">
      <layout xsi:type="JsonLayout">
        <attribute name="time" layout="${longdate}" />
        <attribute name ="elapsedtime" layout="${elapsedtime}" />
        <attribute name="level" layout="${level:upperCase=true}" />
        <attribute name="message" layout="${message}" />
        <attribute name="processid" layout="${processid}" />
        <attribute name="processname" layout="${processname:fullName=true}" />
        <attribute name="processinfo-MachineName" layout="${processinfo:property=MachineName}" />

        <attribute name="processinfo-TotalProcessorTime" layout="${processinfo:property=TotalProcessorTime}" />
        <attribute name="processinfo-PagedMemorySize" layout="${processinfo:property=PagedMemorySize}" />
        <attribute name="processinfo-MaxWorkingSet" layout="${processinfo:property=MaxWorkingSet}" />
        <attribute name="processinfo-MachineNameMinWorkingSet" layout="${processinfo:property=MinWorkingSet}" />
        <attribute name="processinfo-PrivateMemorySize" layout="${processinfo:property=PrivateMemorySize}" />
        <attribute name="processinfo-StartTime" layout="${processinfo:property=StartTime}" />
        <!--<attribute name="processinfo-ExitTime" layout="${processinfo:property=ExitTime}" />-->

        <attribute name="processinfo-WorkingSet64" layout="${processinfo:property=WorkingSet64}" />
        <attribute name="processinfo-WorkingSet" layout="${processinfo:property=WorkingSet}" />
      </layout>
    </target>

    <target name="jsPerformance" xsi:type="File" fileName="${logDirectory}/Json/${shortdate}_all.json">
      <layout xsi:type="JsonLayout">

        <attribute name="time" layout="${longdate}" />
        <attribute name="date" layout="${shortdate}" />
        <attribute name="logger" layout="${logger}" />
        <attribute name="elapsedtime" layout="${elapsedtime}" />
        <attribute name="message" layout="${message}" />
        <attribute name="processtime" layout="${processtime}" />
      </layout>
    </target>
  </targets>

  <rules>

    <!--<logger name="*" minlevel="Debug" writeTo="logfile,logfile2,coloredConsole,viewer" />-->

    <logger name="*" minlevel="Debug" writeTo="console,logfileSplit,consoleLogFile" />

    <!--<logger name="Debug" minlevel="Debug" writeTo="logfile" />-->
    <logger name="General" minlevel="Debug" writeTo="DbLog,jsPerformance" />
    <logger name="Core" minlevel="Debug" writeTo="logfileCore,jsonFileCore" />
    <logger name="Database" minlevel="Debug" writeTo="logfileDatabase" />

    <logger name="RawMsSQL" minlevel="Trace" writeTo="rawLogger" />
    <logger name="RawMySQL" minlevel="Trace" writeTo="rawLogger" />
    <logger name="RawOracle" minlevel="Trace" writeTo="rawLogger" />
    <logger name="RawSqlLite" minlevel="Trace" writeTo="rawLogger" />

    <logger name="Process" minlevel="Debug" writeTo="jsPerformance,logfilePerformanceProcess" />
    <logger name="MsSQL" minlevel="Debug" writeTo="DbLog,jsPerformance,logfilePerformance,logfileAllPerformance" />
    <logger name="MySQL" minlevel="Debug" writeTo="DbLog,jsPerformance,logfilePerformance,logfileAllPerformance" />
    <logger name="Oracle" minlevel="Debug" writeTo="DbLog,jsPerformance,logfilePerformance,logfileAllPerformance" />
    <logger name="SqlLite" minlevel="Debug" writeTo="DbLog,jsPerformance,logfilePerformance,logfileAllPerformance" />

    <logger name="JsonMsSQL" minlevel="Debug" writeTo="jsPerformance,jsonFile"></logger>
    <logger name="JsonMySQL" minlevel="Debug" writeTo="jsonFile"></logger>
    <logger name="JsonOracle" minlevel="Debug" writeTo="jsonFile"></logger>
  </rules>
</nlog>