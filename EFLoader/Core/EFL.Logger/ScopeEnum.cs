﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.LogMgr
{
   public enum ScopeEnum 
    {
        MsSQL = 0,
        MySQL = 1,
        Oracle =2,
        SqlLite = 3,
        Core =10,
        BlockRun = 11,
        General = 12,
        Process = 13,
        Database = 14,
        RawMsSQL =15,
        RawMySQL =16,
        RawOracle = 17,
        RawSqlLite = 18
    }
}
