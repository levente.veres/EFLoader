﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.Monitors.Model
{
    public class EnviromentModel
    {
        public string CategoryName { get; set; }
        public string MachineName { get; set; }
        public DateTime CollectDate { get; set; }
        public Dictionary<HwItem, string> HardwareInfo { get; set; }
        public Dictionary<DbParams, string> DatabaseInfo { get; set; }
    }
}
