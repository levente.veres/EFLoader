﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.Monitors.Model
{
    public enum DbParams
    {
        Type,
        Name,
        Product,
        OS,
        Platform,
        Version,
        Memory,
        Processor        
    }
}
