﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFL.Monitors.Model
{
    public enum HwItem
    {
        ProcessorName,
        ProcessorNr,
        ProcessorClock,
        ProcessorAvr,
        HddCount,
        HddDetails,
        MemoryModules,
        MemorySize,
        MemoryFreeSize,
    }
}
