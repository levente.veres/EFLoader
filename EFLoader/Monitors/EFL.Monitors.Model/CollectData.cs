﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace EFL.Monitors.Model
{

    [Serializable]
    public class CollectData
    {
        public EnviromentModel Environment { get; set; }
        public Dictionary<string, long> Counters { get; set; }

    }
}